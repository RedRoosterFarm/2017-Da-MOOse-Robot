package org.usfirst.frc.team5926.robot;

import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.CameraServer;
import com.ctre.*;
import com.ctre.CANTalon.FeedbackDevice;
import com.ctre.CANTalon.TalonControlMode;

// This was the worlds best comment revere it : gidwashere
// here was a man who was better than you
/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot {
	
	CANTalon frontLeft = new CANTalon(0);
	CANTalon rearLeft = new CANTalon(1);
	CANTalon frontRight = new CANTalon(2);
	CANTalon rearRight = new CANTalon(3);
	CANTalon ballIntake = new CANTalon(4);
	CANTalon robotLift = new CANTalon(5);
	CANTalon shooterFlywheel = new CANTalon(6);
	RobotDrive driveTrain = new RobotDrive(frontLeft, rearLeft, frontRight, rearRight);
	Joystick driveStick = new Joystick(0);
	Joystick buttonBoard = new Joystick(1);
	final String defaultAuto = "Cross Base Line Auto (Default)";
	final String defaultAutoWDelay = "Cross Base Line Auto With Delay";
	final String gearCenterLiftAuto = "Center Gear Lift Auto";
	final String gearAuto1 = "Gear Auto 1";
	String autoSelected;
	SendableChooser<String> chooser = new SendableChooser<>();
	Timer timer = new Timer();
	Solenoid sol1 = new Solenoid(1); // Gear Claw Gripper
	Solenoid sol2 = new Solenoid(0); // Gear Claw Lift
	Solenoid sol3 = new Solenoid(4); // Shooter Trigger
	Solenoid ringLightCircut = new Solenoid(2); // Camera Light
	boolean clawLiftIsUp;
	boolean clawBoolLastState;
	boolean shooterBoolLastState;
	boolean shooterOn;
	boolean ringLightOn;
	
	boolean shooterBoolLastStateA;
	boolean shooterOnA;
	boolean shooterBoolLastStateB;
	boolean shooterOnB;
	
	double autoDelay;

	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	
	@Override
	public void robotInit() {
		chooser.addDefault("Cross Base Line", defaultAuto);
		chooser.addDefault("Cross Base Line (With Delay)", defaultAutoWDelay);
		chooser.addDefault("Center Gear Lift Auto", gearCenterLiftAuto);
		//chooser.addObject("Gear Auto 1", gearAuto1);
		SmartDashboard.putData("Auto choices", chooser);
		SmartDashboard.putNumber("Auto Delay", autoDelay);
		CameraServer.getInstance().startAutomaticCapture(0);
		clawLiftIsUp = true;
		shooterOn = false;
		shooterInit();
	}
	
	public void shooterInit() {
		// first choose the sensor
		shooterFlywheel.setFeedbackDevice(FeedbackDevice.CtreMagEncoder_Relative);
		shooterFlywheel.reverseSensor(false);
		// Set the peak and nominal outputs, 12V means full
		shooterFlywheel.configNominalOutputVoltage(+0.0f, -0.0f);
		shooterFlywheel.configPeakOutputVoltage(+12.5f, -12.5f);
        // set closed loop gains in slot0 
		shooterFlywheel.setProfile(0);
		shooterFlywheel.setF(0.2481);
		shooterFlywheel.setP(0.0731);
		shooterFlywheel.setI(0); 
        shooterFlywheel.setD(0);
        shooterFlywheel.enableBrakeMode(false);
        
	}
	
	// Gear Grabbing Pneumatics
	public void clawOpen(boolean open) {
		if (open == true) { 
			sol1.set(true); // Open Gear Claw
			SmartDashboard.putBoolean("claw", true);
		}
		else {
			sol1.set(false); // Close Gear Claw
			SmartDashboard.putBoolean("claw", false);
		}
		
	}
	
	// Gear Lifting Pneumatics
	public void clawLift(boolean onButton) {
		if (onButton != clawBoolLastState && onButton == true) {
			if (clawLiftIsUp == false ) {
				sol2.set(false); // Raise Gear Claw Lift	
				clawLiftIsUp = true;
				SmartDashboard.putBoolean("clawLift", false);
			}
			else {
				sol2.set(true); // Lower Gear Claw Lift
				clawLiftIsUp = false;
				SmartDashboard.putBoolean("clawLift", true);
			}
		}
		clawBoolLastState = onButton;
	}
	
	public void clawLift2(boolean onButton) {			
			if (onButton == true) {
				sol2.set(true); // Lower Gear Claw Lift
				clawLiftIsUp = false;
				SmartDashboard.putBoolean("clawLift", true);
			}
			else {
				sol2.set(false); // Raise Gear Claw Lift	
				clawLiftIsUp = true;
				SmartDashboard.putBoolean("clawLift", false);
			}
	}
	
	// Ring Light
	public void ringLight(boolean onButton) {
		if (onButton != clawBoolLastState && onButton == true) {
			if (ringLightOn == false ) {
				ringLightCircut.set(true); // ringLight On	
				ringLightOn = true;
				SmartDashboard.putBoolean("ringLight", true);
			}
			else {
				ringLightCircut.set(false); // ringLight Off
				ringLightOn = false;
				SmartDashboard.putBoolean("ringLight", false);
			}
		}
		clawBoolLastState = onButton;
	}
	
	// Shooting Pneumatics
	public void shooterTrigger(boolean open) {
		if (open == true && shooterOnA == true || open == true && shooterOnB == true) { 
			sol3.set(true); // Open Trigger
			SmartDashboard.putBoolean("shooterTrigger", true);
		}
		else {
			sol3.set(false); // Close Trigger
			SmartDashboard.putBoolean("shooterTrigger", false);
		}

	}
	
	// Shooter Flywheel
	/*public void shooter(boolean onButton, double targetSpeed) {
		if (onButton != shooterBoolLastState && onButton == true) {
			if (shooterOn == false) {
				shooterFlywheel.changeControlMode(TalonControlMode.Speed);
				shooterFlywheel.set(targetSpeed);
				shooterOn = true;
				SmartDashboard.putBoolean("shooter", true);
			}
			else {
				shooterFlywheel.set(0.0);
				shooterOn = false;
				SmartDashboard.putBoolean("shooter", false);
			}
		}
		shooterBoolLastState = onButton;
	}*/
	
	// Shooter Flywheel
	public void shooter(boolean onButtonA, double targetSpeedA, boolean onButtonB, double targetSpeedB) {
		if (onButtonA != shooterBoolLastStateA && onButtonA == true && shooterOnB == false) {
			if (shooterOnA == false) {
				shooterFlywheel.changeControlMode(TalonControlMode.Speed);
				shooterFlywheel.set(targetSpeedA);
				shooterOnA = true;
				SmartDashboard.putBoolean("shooter", true);
			}
			else {
				shooterFlywheel.set(0.0);
				shooterOnA = false;
				SmartDashboard.putBoolean("shooter", false);
			}
		}
		if (onButtonB != shooterBoolLastStateB && onButtonB == true && shooterOnA == false) {
			if (shooterOnB == false) {
				shooterFlywheel.changeControlMode(TalonControlMode.Speed);
				shooterFlywheel.set(targetSpeedB);
				shooterOnB = true;
				SmartDashboard.putBoolean("shooter", true);
			}
			else {
				shooterFlywheel.set(0.0);
				shooterOnB = false;
				SmartDashboard.putBoolean("shooter", false);
			}
		}
		shooterBoolLastStateA = onButtonA;
		shooterBoolLastStateB = onButtonB;
		SmartDashboard.putNumber("shooterSpeed", shooterFlywheel.getSpeed());
	}
	
	// Fuel Intake
	public void intake(boolean onFoward, boolean onReverse) {
		if (onFoward == true) {
			ballIntake.set(-0.7);
		}
		else if (onReverse == true) {
			ballIntake.set(0.5);
		}
		else {
			ballIntake.set(0.0);
		}
	}
	
	// 
	public void ropeClimber(boolean onSlow, boolean onFast) {
		if (onFast == true) {
			robotLift.set(-0.65);
		}
		else if (onSlow == true) {
			robotLift.set(-0.25);
		}
		else {
			robotLift.set(0.0);
		}
	}
	
	public void defaultAuto() {
		if (timer.get() <= 1.0) {
			driveTrain.tankDrive(-0.5, 0.5, false);
		}
		else {
			driveTrain.tankDrive(0, 0, false);
		}
	}
	public void defaultAutoWDelay(double Delay) {
		if (timer.get() >= Delay && timer.get() <= 1.5 + Delay) {
			driveTrain.tankDrive(-0.5, 0.5, false);
		}
		else {
			driveTrain.tankDrive(0, 0, false);
		}
	}
	
	public void gearCenterLiftAuto() {
		if (timer.get() <= 2.0) {
			driveTrain.tankDrive(-0.45, 0.45, false);
		}
		else if (timer.get() >= 2.0 && timer.get() <= 2.5) {
			driveTrain.tankDrive(0, 0, false);
		}
		else if (timer.get() >= 2.5 && timer.get() <= 13.0) {
			clawOpen(true);
			driveTrain.tankDrive(0, 0, false);
		}
		else {
			clawOpen(false);
			driveTrain.tankDrive(0, 0, false);
		}
	}
	
	//check these please gearRightLiftAuto() and gearLeftLiftAuto() :Gideon
	
	public void gearRightLiftAuto() {
		if (timer.get() <= 2.0) {
			driveTrain.tankDrive(-0.5, -0.5, false);
		}
			
			
		else if(timer.get() >= 2.0 && timer.get() <= 3.0){
			driveTrain.tankDrive(0.5, -0.5, false);
		}
			
		else if (timer.get() >= 3.0 && timer.get() <= 10.0) {
			clawOpen(true);
			driveTrain.tankDrive(0, 0, false);
		}
			
			
		else {
			clawOpen(false);
			driveTrain.tankDrive(0, 0, false);
		}
	}
	
	public void gearLeftLiftAuto() {
		if (timer.get() <= 2.0) {
			driveTrain.tankDrive(-0.5, -0.5, false);
		}
		
		else if(timer.get() >= 2.0 && timer.get() <= 3.0){
			driveTrain.tankDrive(-0.5, 0.5, false);
		}
		
		else if (timer.get() >= 3.0 && timer.get() <= 10.0) {
			clawOpen(true);
			driveTrain.tankDrive(0, 0, false);
		}
		
		
		else {
			clawOpen(false);
			driveTrain.tankDrive(0, 0, false);
		}
	}

	/**
	 * This autonomous (along with the chooser code above) shows how to select
	 * between different autonomous modes using the dashboard. The sendable
	 * chooser code works with the Java SmartDashboard. If you prefer the
	 * LabVIEW Dashboard, remove all of the chooser code and uncomment the
	 * getString line to get the auto name from the text box below the Gyro
	 *
	 * You can add additional auto modes by adding additional comparisons to the
	 * switch structure below with additional strings. If using the
	 * SendableChooser make sure to add them to the chooser code above as well.
	 */
	@Override
	public void autonomousInit() {
		timer.reset();
		timer.start();
		autoSelected = chooser.getSelected();
		System.out.println("Auto selected: " + autoSelected);
		ringLightCircut.set(true);
		
	}

	/**
	 * This function is called periodically during autonomous
	 */
	@Override
	public void autonomousPeriodic() {
		switch (autoSelected) {
		//case gearAuto1: // Put custom auto code here
		//	break;
		case gearCenterLiftAuto: // Put custom auto code here
			gearCenterLiftAuto();
			break;
		case defaultAutoWDelay: // Put custom auto code here
			defaultAutoWDelay(3.0);
			break;
		case defaultAuto:
		default: // Put default auto code here
			defaultAuto();
			break;
		}
	}

	/**
	 * This function is called periodically during operator control
	 */
	
	@Override
	public void teleopInit() {
		ringLightCircut.set(false);
	}
	
	@Override
	public void teleopPeriodic() {
		if (driveStick.getRawButton(2) == true ){
			driveTrain.arcadeDrive(driveStick.getX()*0.75, -driveStick.getY()*0.5);
		}
		else {
			driveTrain.arcadeDrive(driveStick.getX()*0.95, -driveStick.getY()*0.95);
		}
		
		
		clawOpen(buttonBoard.getRawButton(1));
		
		clawLift2(buttonBoard.getRawButton(3));
		
		intake(buttonBoard.getRawButton(4), buttonBoard.getRawButton(6));
		
		shooter(buttonBoard.getRawButton(7), 3500, buttonBoard.getRawButton(9), 4000);
		
		shooterTrigger(buttonBoard.getRawButton(8));
		
		ropeClimber(buttonBoard.getRawButton(10), buttonBoard.getRawButton(12));
		
		ringLight(buttonBoard.getRawButton(13));

		
	}

	@Override
	public void disabledPeriodic() {
		clawLiftIsUp = true;
		shooterOn = false;
	}

	/**
	 * This function is called periodically during test mode
	 */
	
	@Override
	public void testPeriodic() {
	}
}